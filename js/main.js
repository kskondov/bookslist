$(document).ready(function() {


    var listOfBooks = [];
    var addCounter = 1;
    $("#addBooksBtn").on("click", function() {

        $("#searchResults").hide();
        $("#booksList").show();
        listOfBooks = booksGenerator($("#numberOfBooksToAdd").val());

        listOfBooks.forEach(function(book) {

            $("#booksList").append($("<tr><td class='index'>" +
                addCounter + "</td><td class='title'>" +
                book.title + "</td><td class='description'>" +
                book.description + "</td><td class='category'>" +
                book.category + "</td><tr>"));
            addCounter++;
        });

    });

    let searchResults = new Set();
    $("#searchBtn").on("click", function() {

        if ($("#searchText").val() !== '') {
            searchResults.clear();
            if ((!$('#searchByTitle').is(':checked')) && (!$('#searchByDescription').is(':checked')) && (!$('#searchByCategory').is(':checked'))) {
                $('#searchByTitle').trigger('click');
                $('#searchByDescription').trigger('click');
                $('#searchByCategory').trigger('click');
            }
            $("#searchResults").empty();
            $("#searchResults").append("<tr><th>#</th><th>Title</th><th>Description</th><th>Category</th></tr>");

            $("#booksList").hide();
            if ($('#searchByTitle').is(':checked')) {
                listOfBooks.forEach(function(book) {
                    if (book.title.indexOf($("#searchText").val()) !== -1) {
                        searchResults.add(book);
                    }
                });
            }
            if ($('#searchByDescription').is(':checked')) {
                listOfBooks.forEach(function(book) {
                    if (book.description.indexOf($("#searchText").val()) !== -1) {
                        searchResults.add(book);
                    }
                });
            }
            if ($('#searchByCategory').is(':checked')) {
                listOfBooks.forEach(function(book) {
                    if (book.category.indexOf($("#searchText").val()) !== -1) {
                        searchResults.add(book);
                    }
                });
            }
            var searchCount = 1;

            searchResults.forEach(function(book) {
                $("#searchResults").append($("<tr><td class='index'>" +
                    searchCount + "</td><td class='title'>" +
                    book.title + "</td><td class='description'>" +
                    book.description + "</td><td class='category'>" +
                    book.category + "</td><tr>"));
                searchCount++;

            });

            $('tr td:first').each(function(i) {
                $(this).html(i + 1);
            });
            $("#resetBtn").show();
            $("#books").text("Found Books");
            $("#searchResults").show();
        }

    });

    $("#resetBtn").on("click", function() {
        searchResults.clear();
        $("#books").text("Books List");
        $("#searchResults").hide();
        $("#booksList").show();
        $("#resetBtn").hide();


    });


});

function booksGenerator(listLength) {
    var list = [];
    for (var i = 0; i < listLength; i++) {
        var book = {
            title: randomize(10),
            description: randomize(25),
            category: randomize(5)
        };

        list.push(book);
    }

    return list;

}



function randomize(length) {
    var text = "";
    var lettersAndNumbers = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < length; i++)
        text += lettersAndNumbers.charAt(Math.floor(Math.random() * lettersAndNumbers.length));

    return text;

}